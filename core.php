<?php
require_once('core/settings.php');
require_once('core/lib.php');
require_once('core/DB.php');

session_start();

if($debug){
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

connect();

if(!isset($_SESSION['is_auth'])){
	$menuAuth = [
		['url' => $settings['root_url'].'login.php', 'title' => 'login'],
		['url' => $settings['root_url'].'register.php', 'title' => 'register']
	];
}else{

	$user = getUserByID($_SESSION['user_id']);
	if (getRoleByUserID($_SESSION['user_id']) == 'admin' ){
		$menuAuth = [
		['url' => $settings['root_url'].'logout.php', 'title' => 'logout'],
		['url' => $settings['root_url'].'admin', 'title' => 'Hello, '.$user['name']]
	];
	}else{
		$menuAuth = [
		['url' => $settings['root_url'].'logout.php', 'title' => 'Hello, '.$user['name'].'<br>'.' Logout']
	];
	}
	
	
}