<?php

function redirect($url){
	header('Location:'.$url);
	exit();
}

function isAdmin($role){
	global $usersRoles;

	return $usersRoles['admin'] == $role;
}
