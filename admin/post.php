<?php
require_once('../core.php');

//if(!isset($_SESSION['is_auth']) || !isAdmin($role)) redirect($settings['root_url']);

if(isset($_POST['action'])){
	$action = $_POST['action'];

	switch($action){
		case 'edit':
			updatePost($_POST);

			$categoryPost = getCategoryByPostID($_POST['post_id'])['title'];

			if($_POST['new_category_id'] != $categoryPost) updateCategory($_POST['post_id'], $_POST['new_category_id']);
			
			redirect($settings['root_url'].'admin');
			break;
		case 'remove':
			deletePost($_POST['post_id']);
			redirect($settings['root_url'].'admin');
			break;
		case 'add':
			//$settings['root_url'].'admin/post.php?action=add&status='
			addPost($_POST);
			redirect($settings['root_url'].'admin');
			break;
	}
}

if(isset($_GET['action'])){
	$action = $_GET['action'];

	switch($action){
		case 'view':
			$postView = getPostByID($_GET['post_id']);
			$postView['category'] = getCategoryByPostID($postView['id'])['title'];
			require_once('view/post/view.php');
			break;
		case 'edit':
			$postView = getPostByID($_GET['post_id']);
			$postView['category'] = getCategoryByPostID($postView['id'])['title'];
			$category = getCategorys();
			require_once('view/post/edit.php');
			break;
		case 'remove':
			$postView = getPostByID($_GET['post_id']);
			require_once('view/post/delete.php');
			break;
		case 'add':
			$category = getCategorys();
			$status = (!empty($_GET['status']) ? $_GET['status'] : '');
			require_once('view/post/add.php');
			break;
	}
} 
