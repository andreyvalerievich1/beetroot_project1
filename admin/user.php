<?php
require_once('../core.php');
$role = getRoleByUserID($_SESSION['user_id']);

if(!isset($_SESSION['is_auth']) || !isAdmin($role)) redirect($settings['root_url']);

if(isset($_POST['action'])){
	$action = $_POST['action'];

	switch($action){
		case 'edit':
			updateUser($_POST);

			$roleUser = getRoleByUserID($_POST['user_id'], 'role_id');

			if($_POST['new_role_id'] != $roleUser) updateRole($_POST['user_id'], $_POST['new_role_id']);
			
			redirect($settings['root_url'].'admin');
			break;
		case 'remove':
			deleteUser($_POST['user_id']);
			redirect($settings['root_url'].'admin');
			break;
		case 'add':
			addUser($_POST, $settings['root_url'].'admin/user.php?action=add&status=');
			break;
	}
}

if(isset($_GET['action'])){
	$action = $_GET['action'];

	switch($action){
		case 'view':
			$userView = getUserByID($_GET['user_id']);
			$userView['role'] = getRoleByUserID($userView['id']);
			require_once('view/user/view.php');
			break;
		case 'edit':
			$userView = getUserByID($_GET['user_id']);
			$userView['role'] = getRoleByUserID($userView['id']);
			$roles = getRoles();
			require_once('view/user/edit.php');
			break;
		case 'remove':
			$userView = getUserByID($_GET['user_id']);
			require_once('view/user/delete.php');
			break;
		case 'add':
			$roles = getRoles();
			$status = (!empty($_GET['status']) ? $_GET['status'] : '');
			require_once('view/user/add.php');
			break;
	}
}