<?php {?>
	<div>
		<h2>Posts view</h2>
		<p><a href="<?php echo $settings['root_url'].'admin/';?>">admin</a></p>
	</div>

	<table style="border-collapse: collapse;" cellpadding="10">
		<form method="POST" action="<?php echo $settings['root_url'].'admin/post.php';?>">
			<tr>
				<input type="hidden" name="action" value="edit"/>
				<input type="hidden" name="post_id" value="<?php echo $postView['id'];?>"/>
				<td>Title</td>
				<td><input type="text" name="title" value="<?php echo $postView['title'];?>"></td>
			</tr>
			<tr>
				<td>Content</td>
				<td><input type="text" name="text" value="<?php echo $postView['text'];?>"></td>
			</tr>
			<tr>
				<td>Category</td>
				<td>
					<select name="new_category_id">
						<?php foreach($category as $cat){?>
						<option value="<?php echo $cat['id'];?>" <?php if($cat['title'] == $postView['category']){?>selected<?php }?>><?php echo $cat['title'];?></option>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="edit" name="send_edit"></td>
			</tr>
		</form>
	</table>
<?php }?>
