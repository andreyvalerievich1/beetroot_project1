	<div>
		<h2>Add new post</h2>
		<p><a href="<?php echo $settings['root_url'].'admin/';?>">admin</a></p>
	</div>

	<?php if(isset($status) && !empty($status)){?>
		<div class="msg status"><?php echo $status; ?></div>
	<?php }?>

	<table style="border-collapse: collapse;" cellpadding="10">
		<form method="POST" action="<?php echo $settings['root_url'].'admin/post.php';?>">
			<tr>
				<input type="hidden" name="action" value="add"/>
				<td>Title*</td>
				<td><input type="text" name="title" required></td>
			</tr>
			<tr>
				<td>Text*</td>
				<td><input type="text" name="text"  required></td>
			</tr>
			<tr>
				<td>Category</td>
				<td>
					<select name="category_id">
						<?php foreach($category as $cat){?>
						<option value="<?php echo $cat['id'];?>"><?php echo $cat['title'];?></option>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="add" name="send_add"></td>
			</tr>
		</form>
	</table>