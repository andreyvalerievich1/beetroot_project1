<link rel="stylesheet" href="view/assets/css/style.css">

<nav>
	<a href="<?php echo $settings['root_url']; ?>">Home</a>
</nav>
<main>
		<section class="section">
			<h2>Posts edit</h2>
			<div>
				<p>
					<a class="btn" href="<?php echo $settings['root_url'].'admin/post.php?action=add'; ?>">Add new post</a>
					</p>
						<?php foreach($posts as $post) {?>
					<p>
						<a href="<?php echo $settings['root_url'].'admin/post.php?action=view&post_id='.$post['id']; ?>"><?php echo $post['title'];?></a>
						<a href="<?php echo $settings['root_url'].'admin/post.php?action=edit&post_id='.$post['id']; ?>">edit</a>
						<a href="<?php echo $settings['root_url'].'admin/post.php?action=remove&post_id='.$post['id']; ?>">remove</a>
				</p>
				<?php }?>
			</div>
		</section>

	<?php if(isAdmin($role)){?>
		<section class="section">
			<h2>Users</h2>
			<div>
				<p>
					<a class="btn" href="<?php echo $settings['root_url'].'admin/user.php?action=add'; ?>">Add new user</a>
				</p>
				<?php foreach($users as $user) {?>
					<p>
						<a href="<?php echo $settings['root_url'].'admin/user.php?action=view&user_id='.$user['id']; ?>"><?php echo $user['login'];?></a>
						<a href="<?php echo $settings['root_url'].'admin/user.php?action=edit&user_id='.$user['id']; ?>">edit</a>
						<a href="<?php echo $settings['root_url'].'admin/user.php?action=remove&user_id='.$user['id']; ?>">remove</a>
					</p>
				<?php }?>
			</div>
		</section>
	<?php }?>
</main>