<?php if(isAdmin($role)){?>
	<div>
		<h2>User view</h2>
		<p><a href="<?php echo $settings['root_url'].'admin/';?>">admin</a></p>
	</div>

	<table style="border-collapse: collapse;" cellpadding="10">
		<form method="POST" action="<?php echo $settings['root_url'].'admin/user.php';?>">
			<tr>
				<input type="hidden" name="action" value="edit"/>
				<input type="hidden" name="user_id" value="<?php echo $userView['id'];?>"/>
				<td>Login</td>
				<td><input type="text" name="login" value="<?php echo $userView['login'];?>"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" name="email" value="<?php echo $userView['email'];?>"></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><input type="text" name="name" value="<?php echo $userView['name'];?>"></td>
			</tr>
			<tr>
				<td>Date Of Birthday</td>
				<td><input type="text" name="dob" value="<?php echo $userView['dob']; ?>"></td>
			</tr>
			<tr>
				<td>Role</td>
				<td>
					<select name="new_role_id">
						<?php foreach($roles as $role){?>
						<option value="<?php echo $role['id'];?>" <?php if($role['title'] == $userView['role']){?>selected<?php }?>><?php echo $role['title'];?></option>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="edit" name="send_edit"></td>
			</tr>
		</form>
	</table>
<?php }?>