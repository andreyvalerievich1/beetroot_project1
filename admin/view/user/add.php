<?php if(isAdmin($role)){?>
	<div>
		<h2>Add new user</h2>
		<p><a href="<?php echo $settings['root_url'].'admin/';?>">admin</a></p>
	</div>

	<?php if(isset($status) && !empty($status)){?>
		<div class="msg status"><?php echo $status; ?></div>
	<?php }?>

	<table style="border-collapse: collapse;" cellpadding="10">
		<form method="POST" action="<?php echo $settings['root_url'].'admin/user.php';?>">
			<tr>
				<input type="hidden" name="action" value="add"/>
				<td>Login*</td>
				<td><input type="text" name="login" required></td>
			</tr>
			<tr>
				<td>Password*</td>
				<td><input type="password" name="pass"  required></td>
			</tr>
			<tr>
				<td>Confirm password*</td>
				<td><input type="password" name="pass_conf" required></td>
			</tr>
			<tr>
				<td>Email*</td>
				<td><input type="email" name="mail"  required></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<td>Date Of Birthday</td>
				<td><input type="text" name="dob"></td>
			</tr>
			<tr>
				<td>Role</td>
				<td>
					<select name="role">
						<?php foreach($roles as $role){?>
						<option value="<?php echo $role['id'];?>"><?php echo $role['title'];?></option>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="add" name="send_add"></td>
			</tr>
		</form>
	</table>
<?php }?>