<?php
require_once('core.php');
if(isset($_SESSION['is_auth'])) redirect($settings['root_url']);

$menuAuth = (isset($menuAuth) ? $menuAuth : []);

if(!empty($_POST['send_login'])){
	$status;

	if(empty($_POST['login'])){
		$status = 'Поле пользователя пустое';
		redirect($settings['root_url'].'login.php?status='.$status);
	} else if(empty($_POST['pass'])){
		$status = 'Поле пароль пустое';
		redirect($settings['root_url'].'login.php?status='.$status);
	}else{
		$login = $_POST['login'];
		$pass = ($_POST['pass']);

		$user = getUserByPass($login, $pass);

		if(!empty($user['id'])){
			$_SESSION['is_auth'] = true;
			$_SESSION['user_id'] = $user['id'];
			redirect($settings['root_url']);
		}else{
			$status = 'Такого пользователя не существует';
			redirect($settings['root_url'].'login.php?status='.$status);
		}
	}
}

$status = (!empty($_GET['status']) ? $_GET['status'] : '');

require_once('view/login.php');