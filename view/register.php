<?php if(isset($status) && !empty($status)){?>
	<div class="msg status"><?php echo $status; ?></div>
<?php }?>

<nav>
	<ul>
		<?php foreach($menuAuth as $item){ ?>
			<li><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
		<?php }?>
	</ul>
</nav>

<div>
	<form method="POST" action="">
		<table>
			<tr>
				<td><label>Login:</label></td>
				<td><input type="text" name="login" required/></td>
			</tr>
			<tr>
				<td><label>Name:</label></td>
				<td><input type="text" name="name" required/></td>
			</tr>
			<tr>
				<td><label>Password:</label></td>
				<td><input type="password" name="pass" required/></td>
			</tr>
			<tr>
				<td><label>Confirm password:</label></td>
				<td><input type="password" name="pass_conf" required/></td>
			</tr>
			<tr>
				<td><label>Email:</label></td>
				<td><input type="email" name="mail" required/></td>
			</tr>
			<tr>
				<td><label>Role:</label></td>
				<td>
					<select name="role" required>
						<?php foreach($roles as $role){?>
						<option value="<?php echo $role['id']; ?>"><?php echo $role['title']; ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="add_user" value="Register"/>
				</td>
			</tr>
		</table>
	</form>
</div>