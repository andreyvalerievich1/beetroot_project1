<?php if(isset($status) && !empty($status)){?>
	<div class="msg status"><?php echo $status; ?></div>
<?php }?>

<nav>
	<ul>
		<?php foreach($menuAuth as $item){ ?>
			<li><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
		<?php }?>
	</ul>
</nav>

<div>
	<form method="POST" action="">
		<table>
			<tr>
				<td><label>Login:</label></td>
				<td><input type="text" name="login" required/></td>
			</tr>
			<tr>
				<td><label>Password:</label></td>
				<td><input type="password" name="pass" required/></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="send_login" value="Login"/>
				</td>
			</tr>
		</table>
	</form>
</div>