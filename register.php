<?php
require_once('core.php');
if(isset($_SESSION['is_auth'])) redirect($settings['root_url']);

if(!empty($_POST) && isset($_POST['add_user'])){
	addUser($_POST);
}

$status = (!empty($_GET['status']) ? $_GET['status'] : '');

$menuAuth = (isset($menuAuth) ? $menuAuth : []);

$roles = getRoles();

require_once('view/register.php');