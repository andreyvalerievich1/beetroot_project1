<?php 
class PostNews extends Post
{
	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($val)
	{
		if(empty($val)) return;
		$this->title = $val;
	}

	public function getDesc()
	{
		return $this->desc;
	}

	public function setDesc($val)
	{
		if(empty($val)) return;
		$this->desc = $val;
	}

	public function getCategory()
	{
		return $this->category;
	}

	public function setCategory($val)
	{
		if(empty($val)) return;
		$this->category = $val;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setDate($val)
	{
		if(empty($val)) return;
		$this->date = $val;
	}

}