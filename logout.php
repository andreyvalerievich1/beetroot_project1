<?php
require_once('core.php');

$is_redirect = false;

if(isset($_SESSION['is_auth'])){
	unset($_SESSION['is_auth']);
	$is_redirect = true;
}

if(isset($_SESSION['user_id'])){
	unset($_SESSION['user_id']);
	$is_redirect = true;
}

if($is_redirect) redirect($settings['root_url']);